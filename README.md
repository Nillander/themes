# Themes

[![N|Solid](https://cldup.com/dTxpPi9lDf.thumb.png)](https://bitbucket.org/nillandereng/themes)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://bitbucket.org/nillandereng/themes)

Repositório de temas

  - Bullsy - http://bullsy.premiumcoding.com/
  - Chromatron - https://themeforest.net/item/chromatron-html5-admin-backend/234784
  - Gentelella - https://github.com/puikinsh/gentelella
  - Metronic 3.6.1 - https://keenthemes.com/metronic/
  - Metronic 4.6 - https://keenthemes.com/metronic/

# Novas Implementações!

  - Import a HTML file and watch it magically convert to Markdown
  - Drag and drop images (requires your Dropbox account be linked)

## Trecho de código

Exemplo de  [link por variavel][variavel]
Abaixo é exibido um código em caixa de código

```SQL
use `aes_databasename`;
show tables;
```

Não esquecer de informar o nome da linguagem após as 4 aspas simples a esquerdar

### Tabelas

Exemplo de tabela.

| Plugin | README |
| ------ | ------ |
| Dropbox | [plugins/dropbox/README.md][PlDb] |
| Github | [plugins/github/README.md][PlGh] |
| Google Drive | [plugins/googledrive/README.md][PlGd] |
| OneDrive | [plugins/onedrive/README.md][PlOd] |
| Medium | [plugins/medium/README.md][PlMe] |
| Google Analytics | [plugins/googleanalytics/README.md][PlGa] |




#### Nome + Link

Veja o [link direto na palavra](https://github.com/joemccann/dillinger/blob/master/KUBERNETES.md)

**Free Software, Hell Yeah!**

[//]: #


   [variavel]: <https://github.com/joemccann/dillinger>

   [PlDb]: <https://github.com/joemccann/dillinger/tree/master/plugins/dropbox/README.md>
   [PlGh]: <https://github.com/joemccann/dillinger/tree/master/plugins/github/README.md>
   [PlGd]: <https://github.com/joemccann/dillinger/tree/master/plugins/googledrive/README.md>
   [PlOd]: <https://github.com/joemccann/dillinger/tree/master/plugins/onedrive/README.md>
   [PlMe]: <https://github.com/joemccann/dillinger/tree/master/plugins/medium/README.md>
   [PlGa]: <https://github.com/RahulHP/dillinger/blob/master/plugins/googleanalytics/README.md>
