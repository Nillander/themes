Chromatron HTML5 Admin Backend

======================================================

Author:			WalkingPixels | www.walkingpixels.com
Current version: 	2.4
Bootstrap support:	2.3.2

======================================================


Version 2.4
- Support Bootstrap 2.3.2
- Collapsible navigation block
- Plugins added - Daterange picker, Timepicker, File manager, Extended modal, Lightbox, Slimscroll, Switch, Sparklines, Form validation
- Update Wizard and Flot chart plugins
- Customized jQuery-UI library added
- Dashboard, Forms, Tables, File explorer content update
- User profile, Online store, Add product, Invoice pages created
- Built-in template documentation
- Improved Media table and Widgets styling
- Responsive charts
- jQuery plUpload plugin removed
- Minor LESS and CSS tweaks
- fix DataTables on smaller screens


Version 2.3
- Support Bootstrap 2.3.0
- Update LESS 1.3.3
- Fix IE9 selectors issue
- Minor LESS and CSS tweaks


Version 2.2
- Support Bootstrap 2.2.2
- Update Modernizr 2.6.2
- Update LESS.js 1.3.1
- Update Colorpicker, Datepciker, Fileupload, DataTables, Flot Charts plugins
- Add minified Bootstrap javascript
- Improve styling for fileupload
- Better responsive and layout styling
- Fix DataTables image path typo
- Fix background for fixed layouts if there is not enough content
- A lot of LESS/CSS tweaks


Version 2.1
- Support jQuery 1.8.1
- Add Bootstrap File Input plugin
- Add Bootstrap Masked Input plugin
- Add changelog
- Add fixed layout
- Fix dead placehold.it links
- Minor LESS and CSS tweaks


Version 2.0
- Major update and complete rework
- Support Bootstrap 2.1.0 and 2.1.1
- Support jQuery 1.8.0
- Update all jQuery plugins
- Reponsive styles


Version 1.3
- Several bugs fixed


Version 1.2
- New features and plugins
- Minor bugs fixed


Version 1.1
- Several new features and components
- Several new layouts
- CSS refactoring
- Several bugs fixed


Version 1.0
- Initial release